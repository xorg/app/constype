constype prints on the standard output the Sun code for the type of
display that the specified device is.

It was originally written for SunOS, but has been ported to other
SPARC OS'es and to Solaris on both SPARC & x86.

It has lived for years in the xc/programs/Xserver/hw/sun directory of
the X Window System, and is now split out by itself as part of the
X.Org Modularization Project for X11R7.0.

All questions regarding this software should be directed at the
Xorg mailing list:

  https://lists.x.org/mailman/listinfo/xorg

The master development code repository can be found at:

  https://gitlab.freedesktop.org/xorg/app/constype

Please submit bug reports and requests to merge patches there.

For patch submission instructions, see:

  https://www.x.org/wiki/Development/Documentation/SubmittingPatches

